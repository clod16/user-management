package it.clod.user.management.utils;

import it.clod.user.management.model.User;

import java.util.HashMap;
import java.util.Map;

public class UserValidator {


    private static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }




    public static Map<String, Boolean> validate(User user) {

        if (user == null) {
            return null;
        }
        Map<String, Boolean> userErrorMap = new HashMap<String, Boolean>();

        if (user.getName() == null || user.getName().trim().isEmpty()) {
            userErrorMap.put("name", false);
        }
        if (user.getSurname() == null || user.getSurname().trim().isEmpty()) {
            userErrorMap.put("surname", false);
        }


        if (user.getEmail() == null || user.getEmail().trim().isEmpty()) {
            userErrorMap.put("email", false);
        }

        if (isValidEmailAddress(user.getEmail())) {
            userErrorMap.put("email", false);
        }

        if (user.getAddress() == null || user.getAddress().trim().isEmpty()) {
            userErrorMap.put("address", false);
        }

        return userErrorMap;

    }
}
