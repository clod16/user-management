# user-management

Una volta scaricata con git può essere lanciata con il comando :  _`mvn spring-boot:run`_

Inoltre, è possibile andare a questo indirizzo per consultare la pagina **Swagger** e provare eventualmente le API.

Capitolo DB: ho scelto di utilizzare H2, attualmente è preimpostato per lavorare in memory, se si vuole disattivare, commentare la riga 3 del file application.properties ed attivare la seconda riga.

`#spring.datasource.url=jdbc:h2:file:./user` \
`spring.datasource.url=jdbc:h2:mem:user
`


Capitolo Docker: non avendo esperienza in docker ho comunque provato a generale l'immagine. (mi sono aiutato con guide ed articoli online ovviamente).
Comunque dalla root del progetto lanciando il comando 
_`docker build -t user-management-app .`_ viene buildata l'immagine, infine con
_`docker run -p 8080:8080 user-management-app`_
la saluzione viene lanciata


Nella cartella build troverete il JAR dell applicazione