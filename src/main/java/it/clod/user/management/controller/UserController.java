package it.clod.user.management.controller;

import it.clod.user.management.model.User;
import it.clod.user.management.service.UserService;
import it.clod.user.management.utils.UserValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping
    public List<User> getAllUsers() {
        return this.userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id) {

        Optional<User> user = this.userService.getUserById(id);
        if (user.isPresent()) {
            return new ResponseEntity<>(user.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user) {

        if (UserValidator.validate(user).isEmpty()) {
            return new ResponseEntity<>(this.userService.saveUser(user), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> updateUser(@PathVariable Long id, @RequestBody User user) {

        if (UserValidator.validate(user).isEmpty()) {

            Optional<User> existingUser = this.userService.getUserById(id);
            if (existingUser.isPresent()) {
                user.setId(id);
                return new ResponseEntity<>(this.userService.saveUser(user), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        try {
            if (this.userService.getUserById(id).isPresent()) {
                this.userService.deleteUser(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/search")
    public ResponseEntity<List<User>> searchUsers(@RequestParam(required = false) String name,
                                                  @RequestParam(required = false) String surname) {
        List<User> byNameOrSurname = this.userService.findByNameOrSurname(name, surname);
        return new ResponseEntity<>(byNameOrSurname, HttpStatus.OK);
    }


    @PostMapping(value = "/upload-csv", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public ResponseEntity<List<User>> uploadCsvFile(@RequestBody MultipartFile csv) {

        if (csv.getOriginalFilename() == null || csv.getOriginalFilename().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<User> users = this.userService.saveUsersFromCsv(csv);
        return new ResponseEntity<>(users, HttpStatus.CREATED);
    }
}