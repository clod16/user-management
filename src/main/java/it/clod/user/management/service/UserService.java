package it.clod.user.management.service;

import it.clod.user.management.model.User;
import it.clod.user.management.repository.UserRepository;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {


    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public Optional<User> getUserById(Long id) {
        return userRepository.findById(id);
    }

    public User saveUser(User user) {
        return this.userRepository.save(user);
    }

    public List<User> saveUsers(List<User> users) {
        return this.userRepository.saveAll(users);
    }

    public void deleteUser(Long id) {
        this.userRepository.deleteById(id);
    }

    public List<User> saveUsersFromCsv(MultipartFile file) {

        if (file.isEmpty()) {
            throw new IllegalArgumentException("File is empty");
        }

        List<User> users = new ArrayList<>();
        try (BufferedReader fileReader = new BufferedReader(
                new InputStreamReader(file.getInputStream(),
                        StandardCharsets.UTF_8));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim())) {

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                User user = new User();
                user.setName(csvRecord.get("name"));
                user.setSurname(csvRecord.get("surname"));
                user.setEmail(csvRecord.get("email"));
                user.setAddress(csvRecord.get("address"));
                users.add(user);
            }

            return this.userRepository.saveAll(users);
        } catch (Exception e) {
            throw new RuntimeException("Failed to parse CSV file: " + e.getMessage());
        }
    }


    public List<User> findByNameOrSurname(String name, String surname) {

        if(name == null && surname == null) {
            return this.userRepository.findAll();
        }
        return this.userRepository.findByNameOrSurname(name, surname);
    }
}
